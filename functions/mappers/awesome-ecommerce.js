const router = require("express").Router();
const productRoutes = require("../api/routes/awesome-ecommerce-product");

router.get("/products", productRoutes.awesomeEcommerceProduct);

module.exports = router;
